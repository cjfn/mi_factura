class FacturasDetallesController < ApplicationController
  before_action :set_factura_detalle, only: [:show, :edit, :update, :destroy]

  # GET /facturas_detalles
  # GET /facturas_detalles.json
  def index
    @facturas_detalles = FacturaDetalle.all
  end

  # GET /facturas_detalles/1
  # GET /facturas_detalles/1.json
  def show
  end

  # GET /facturas_detalles/new
  def new
    @factura_detalle = FacturaDetalle.new
  end

  # GET /facturas_detalles/1/edit
  def edit
  end

  # POST /facturas_detalles
  # POST /facturas_detalles.json
  def create
    @factura_detalle = FacturaDetalle.new(factura_detalle_params)

    respond_to do |format|
      if @factura_detalle.save #si el detalle de factura se guardo

        total = @factura_detalle.factura.total # total referenciado a la factura

        total += @factura_detalle.total #sumo el total de la factura

        @factura_detalle.factura.update total: total #modifico el total de la factura con el total resultante

        format.js
        format.json { render :show, status: :created, location: @factura_detalle }
      else
        @factura = @factura_detalle.factura #creo variable que apunta al detalle de la factura en la factura
        format.html { render  "/facturas/show"} # muestro el detalle de la factura en la vista sho de facturas
        format.json { render json: @factura_detalle.factura.errors, status: :unprocessable_entity }


      end
    end
  end

  # PATCH/PUT /facturas_detalles/1
  # PATCH/PUT /facturas_detalles/1.json
  def update
    respond_to do |format|
      if @factura_detalle.update(factura_detalle_params)
        format.html { redirect_to @factura_detalle, notice: 'Factura detalle was successfully updated.' }
        format.json { render :show, status: :ok, location: @factura_detalle }
      else
        format.html { render :edit }
        format.json { render json: @factura_detalle.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facturas_detalles/1
  # DELETE /facturas_detalles/1.json
  def destroy
    #al eliminar un detalle de la factura
     factura=@factura_detalle.factura.id #creo variable que apunta al id de la factura que se esta creando

      total = @factura_detalle.factura.total #creo variable con el total que tiene el total de la factura

      total -= @factura_detalle.total #resto el detalle seleccionado del total

      @factura_detalle.factura.update total: total #realizo el update en el total de la factura

    @factura_detalle.destroy
    respond_to do |format|
      format.html { redirect_to factura_path(factura), notice: 'Factura detalle was successfully destroyed.' }
      format.json { head :no_content }

    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_factura_detalle
      @factura_detalle = FacturaDetalle.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def factura_detalle_params
      params.require(:factura_detalle).permit(:factura_id, :producto_id, :cantidad, :precio, :total)
    end
end
