class FirmasFlowsController < ApplicationController
  before_action :set_firmas_flow, only: [:show, :edit, :update, :destroy]

  # GET /firmas_flows
  # GET /firmas_flows.json
  def index
    @firmas_flows = FirmasFlow.all
  end

  # GET /firmas_flows/1
  # GET /firmas_flows/1.json
  def show
  end

  # GET /firmas_flows/new
  def new
    @firmas_flow = FirmasFlow.new
  end

  # GET /firmas_flows/1/edit
  def edit
  end

  # POST /firmas_flows
  # POST /firmas_flows.json
  def create
    @firmas_flow = FirmasFlow.new(firmas_flow_params)

    respond_to do |format|
      if @firmas_flow.save
        format.html { redirect_to @firmas_flow, notice: 'Firmas flow was successfully created.' }
        format.json { render :show, status: :created, location: @firmas_flow }
      else
        format.html { render :new }
        format.json { render json: @firmas_flow.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /firmas_flows/1
  # PATCH/PUT /firmas_flows/1.json
  def update
    respond_to do |format|
      if @firmas_flow.update(firmas_flow_params)
        format.html { redirect_to @firmas_flow, notice: 'Firmas flow was successfully updated.' }
        format.json { render :show, status: :ok, location: @firmas_flow }
      else
        format.html { render :edit }
        format.json { render json: @firmas_flow.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /firmas_flows/1
  # DELETE /firmas_flows/1.json
  def destroy
    @firmas_flow.destroy
    respond_to do |format|
      format.html { redirect_to firmas_flows_url, notice: 'Firmas flow was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_firmas_flow
      @firmas_flow = FirmasFlow.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def firmas_flow_params
      params.require(:firmas_flow).permit(:user_id, :firmas_tipos_documentos_id, :lleva_firma, :tipo, :paso_aceptado, :texto_aceptado, :paso_no_aceptado, :texto_no_aceptado)
    end
end
