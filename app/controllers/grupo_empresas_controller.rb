class GrupoEmpresasController < ApplicationController
  before_action :set_grupo_empresa, only: [:show, :edit, :update, :destroy]

  # GET /grupo_empresas
  # GET /grupo_empresas.json
  def index
    @grupo_empresas = GrupoEmpresa.all
  end

  # GET /grupo_empresas/1
  # GET /grupo_empresas/1.json
  def show
  end

  # GET /grupo_empresas/new
  def new
    @grupo_empresa = GrupoEmpresa.new
  end

  # GET /grupo_empresas/1/edit
  def edit
  end

  # POST /grupo_empresas
  # POST /grupo_empresas.json
  def create
    @grupo_empresa = GrupoEmpresa.new(grupo_empresa_params)

    respond_to do |format|
      if @grupo_empresa.save
        format.html { redirect_to @grupo_empresa, notice: 'Grupo empresa was successfully created.' }
        format.json { render :show, status: :created, location: @grupo_empresa }
      else
        format.html { render :new }
        format.json { render json: @grupo_empresa.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /grupo_empresas/1
  # PATCH/PUT /grupo_empresas/1.json
  def update
    respond_to do |format|
      if @grupo_empresa.update(grupo_empresa_params)
        format.html { redirect_to @grupo_empresa, notice: 'Grupo empresa was successfully updated.' }
        format.json { render :show, status: :ok, location: @grupo_empresa }
      else
        format.html { render :edit }
        format.json { render json: @grupo_empresa.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /grupo_empresas/1
  # DELETE /grupo_empresas/1.json
  def destroy
    @grupo_empresa.destroy
    respond_to do |format|
      format.html { redirect_to grupo_empresas_url, notice: 'Grupo empresa was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_grupo_empresa
      @grupo_empresa = GrupoEmpresa.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def grupo_empresa_params
      params.require(:grupo_empresa).permit(:nombre, :direcion)
    end
end
