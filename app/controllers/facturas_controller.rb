class FacturasController < ApplicationController
  before_action :set_factura, only: [:show, :edit, :update, :destroy]

  # GET /facturas
  # GET /facturas.json
  def index
    @facturas = Factura.all
     respond_to do |format|
      format.html
      format.csv { render text: @facturas.to_csv }
      format.xls { render text: @facturas.to_csv(col_sep: "\t") }
    end
  end


  def nueva_fila
    #validar primero si el archivo @producto.url_pdf existe
    send_file @factura.nueva_fila
  end


   def excel
    #validar primero si el archivo @producto.url_pdf existe

  end

  # GET /facturas/1
  # GET /facturas/1.json
  def show
    @factura_detalle = FacturaDetalle.new #muestro el formulario de detalles de la factura en el detalle
    @factura_detalle.factura_id = @factura.id #muestro los detalles de esta factura en la tabla
    @detfactura = FacturaDetalle.where("factura_id = ?",params[:id])
    respond_to do |format|
      format.html
      format.js
      format.csv { render text: @detfactura.to_csv }
      format.xls { render text: @detfactura.to_csv(col_sep: "\t") }
      format.pdf  {render template: 'facturas/reporte', pdf: 'Reporte'}
    end

    
  end

  # GET /facturas/new
  def new
    @factura = Factura.new
  end

  # GET /facturas/1/edit
  def edit
  end

  # POST /facturas
  # POST /facturas.json
  def create
    @factura = Factura.new(factura_params)#creo variable  nueva con los parametros
    @factura.total=0 #al crear nueva factura establezco por default 0

    respond_to do |format|
      if @factura.save
        format.html { redirect_to @factura, notice: 'Factura creada exitosamente.' }
        format.json { render :show, status: :created, location: @factura }
      else
        format.html { render :new }
        format.json { render json: @factura.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /facturas/1
  # PATCH/PUT /facturas/1.json
  def update
    respond_to do |format|
      if @factura.update(factura_params)
        format.html { redirect_to @factura, notice: 'Factura was successfully updated.' }
        format.json { render :show, status: :ok, location: @factura }
      else
        format.html { render :edit }
        format.json { render json: @factura.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /facturas/1
  # DELETE /facturas/1.json
  def destroy
    @factura.destroy
    respond_to do |format|
      format.html { redirect_to facturas_url, notice: 'Factura was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_factura
      @factura = Factura.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def factura_params
      params.require(:factura).permit(:numero, :fecha, :nit_cliente, :nombre_cliente, :total)
    end
end
