class FirmasTiposDocumentosController < ApplicationController
  before_action :set_firmas_tipos_documento, only: [:show, :edit, :update, :destroy]

  # GET /firmas_tipos_documentos
  # GET /firmas_tipos_documentos.json
  def index
    @firmas_tipos_documentos = FirmasTiposDocumento.all
  end

  # GET /firmas_tipos_documentos/1
  # GET /firmas_tipos_documentos/1.json
  def show
    @firmas_flow = FirmasFlow.new #muestro el formulario de detalles de la factura en el detalle
    @firmas_flow.firmas_tipos_documentos_id = @firmas_tipos_documento.id #muestro los detalles de esta factura en la tabla
  end

  # GET /firmas_tipos_documentos/new
  def new
    @firmas_tipos_documento = FirmasTiposDocumento.new
  end

  # GET /firmas_tipos_documentos/1/edit
  def edit
  end

  # POST /firmas_tipos_documentos
  # POST /firmas_tipos_documentos.json
  def create
    @firmas_tipos_documento = FirmasTiposDocumento.new(firmas_tipos_documento_params)

    respond_to do |format|
      if @firmas_tipos_documento.save
        format.html { redirect_to @firmas_tipos_documento, notice: 'Firmas tipos documento was successfully created.' }
        format.json { render :show, status: :created, location: @firmas_tipos_documento }
      else
        format.html { render :new }
        format.json { render json: @firmas_tipos_documento.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /firmas_tipos_documentos/1
  # PATCH/PUT /firmas_tipos_documentos/1.json
  def update
    respond_to do |format|
      if @firmas_tipos_documento.update(firmas_tipos_documento_params)
        format.html { redirect_to @firmas_tipos_documento, notice: 'Firmas tipos documento was successfully updated.' }
        format.json { render :show, status: :ok, location: @firmas_tipos_documento }
      else
        format.html { render :edit }
        format.json { render json: @firmas_tipos_documento.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /firmas_tipos_documentos/1
  # DELETE /firmas_tipos_documentos/1.json
  def destroy
    @firmas_tipos_documento.destroy
    respond_to do |format|
      format.html { redirect_to firmas_tipos_documentos_url, notice: 'Firmas tipos documento was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_firmas_tipos_documento
      @firmas_tipos_documento = FirmasTiposDocumento.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def firmas_tipos_documento_params
      params.require(:firmas_tipos_documento).permit(:grupo_empresa_id, :nombre, :descripcion, :aceptado)
    end
end
