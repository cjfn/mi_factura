json.extract! firmas_tipos_documento, :id, :grupo_empresa_id, :nombre, :descripcion, :aceptado, :created_at, :updated_at
json.url firmas_tipos_documento_url(firmas_tipos_documento, format: :json)
