json.extract! firmas_flow, :id, :user_id, :firmas_tipos_documentos_id, :lleva_firma, :tipo, :paso_aceptado, :texto_aceptado, :paso_no_aceptado, :texto_no_aceptado, :created_at, :updated_at
json.url firmas_flow_url(firmas_flow, format: :json)
