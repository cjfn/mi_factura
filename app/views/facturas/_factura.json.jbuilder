json.extract! factura, :id, :numero, :fecha, :nit_cliente, :nombre_cliente, :total, :created_at, :updated_at
json.url factura_url(factura, format: :json)
