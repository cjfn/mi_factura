json.extract! producto, :id, :codigo, :descripcion, :precio, :observaciones, :activo, :created_at, :updated_at
json.url producto_url(producto, format: :json)
