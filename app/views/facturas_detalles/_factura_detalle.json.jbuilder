json.extract! factura_detalle, :id, :factura_id, :producto_id, :cantidad, :precio, :total, :created_at, :updated_at
json.url factura_detalle_url(factura_detalle, format: :json)
