# == Schema Information
#
# Table name: facturas_detalles
#
#  id          :integer          not null, primary key
#  factura_id  :integer
#  producto_id :integer
#  cantidad    :integer
#  precio      :decimal(, )
#  total       :decimal(, )
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class FacturaDetalle < ApplicationRecord
	validates_presence_of :producto_id, :cantidad, :precio, :total #requerido
	belongs_to :producto # pertenece a padre
	belongs_to :factura
	self.abstract_class

   def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |facturadetalle|
        csv << facturadetalle.attributes.values_at(*column_names)
      end
    end
  end


    def excel
       Rails.root.join("factura/",self.id.to_s,".xls").to_s#agregarmos la ruta url con el nombre dle archivo
    end
end
