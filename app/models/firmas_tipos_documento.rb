# == Schema Information
#
# Table name: firmas_tipos_documentos
#
#  id               :integer          not null, primary key
#  grupo_empresa_id :integer
#  nombre           :string
#  descripcion      :text
#  aceptado         :boolean
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class FirmasTiposDocumento < ApplicationRecord
	has_many :firmas_flow
	belongs_to :grupo_empresa
	def name
		nombre
	end

end
