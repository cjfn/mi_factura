# == Schema Information
#
# Table name: productos
#
#  id                  :integer          not null, primary key
#  codigo              :string
#  descripcion         :string
#  precio              :decimal(, )
#  observaciones       :text
#  activo              :boolean
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  imagen_file_name    :string
#  imagen_content_type :string
#  imagen_file_size    :integer
#  imagen_updated_at   :datetime
#  pdf_file_name       :string
#  pdf_content_type    :string
#  pdf_file_size       :integer
#  pdf_updated_at      :datetime
#

class Producto < ApplicationRecord
	validates :codigo, uniqueness: true
	validates :codigo, :descripcion, :precio, presence: true
	has_attached_file :imagen, styles: { medium: "300x300", thumb: "100x100" }, default_url: "/images/no-existe.png"
  	validates_attachment_content_type :imagen, content_type: /\Aimage\/.*\z/

  	has_attached_file :pdf, default_url: "/images/no-existe.png", path: ":rails_root/pdf/:id/:filename",   url: ":rails_root/pdf/:id/:filename"

    validates_attachment_content_type :pdf, content_type: ['application/pdf']            	

    def url_pdf
    	 Rails.root.join("pdf/",self.id.to_s,self.pdf_file_name||="nofile.pdf").to_s#agregarmos la ruta url con el nombre dle archivo
    end
    def name
		descripcion
	 end

   def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |producto|
        csv << producto.attributes.values_at(*column_names)
      end
    end
  end
end


