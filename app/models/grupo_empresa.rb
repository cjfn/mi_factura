# == Schema Information
#
# Table name: grupo_empresas
#
#  id         :integer          not null, primary key
#  nombre     :string
#  direcion   :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class GrupoEmpresa < ApplicationRecord
	has_many :firmas_tipos_documento # tiene muchos en plural 
	def name
		nombre
	end
end
