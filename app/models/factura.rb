# == Schema Information
#
# Table name: facturas
#
#  id             :integer          not null, primary key
#  numero         :integer
#  fecha          :date
#  nit_cliente    :string
#  nombre_cliente :string
#  total          :decimal(, )
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#

class Factura < ApplicationRecord
	validates_presence_of :numero, :fecha, :nit_cliente, :nombre_cliente # validacion para requeir valores
	validates_uniqueness_of :numero # validacion sin repetir
	validates_length_of :nombre_cliente, within: 3..255 #validacion de largo requerido permitido
	has_many :facturas_detalles # tiene muchos en plural 


   def self.to_csv(options = {})
    CSV.generate(options) do |csv|
      csv << column_names
      all.each do |factura|
        csv << factura.attributes.values_at(*column_names)
      end
    end
  end
end
