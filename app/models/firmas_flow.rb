# == Schema Information
#
# Table name: firmas_flows
#
#  id                         :integer          not null, primary key
#  user_id                    :integer
#  firmas_tipos_documentos_id :integer
#  lleva_firma                :boolean
#  tipo                       :string
#  paso_aceptado              :integer
#  texto_aceptado             :string
#  paso_no_aceptado           :integer
#  texto_no_aceptado          :string
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#

class FirmasFlow < ApplicationRecord
	belongs_to :user
	belongs_to :firmas_tipos_documento

end
