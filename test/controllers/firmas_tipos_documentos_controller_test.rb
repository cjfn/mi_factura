require 'test_helper'

class FirmasTiposDocumentosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @firmas_tipos_documento = firmas_tipos_documentos(:one)
  end

  test "should get index" do
    get firmas_tipos_documentos_url
    assert_response :success
  end

  test "should get new" do
    get new_firmas_tipos_documento_url
    assert_response :success
  end

  test "should create firmas_tipos_documento" do
    assert_difference('FirmasTiposDocumento.count') do
      post firmas_tipos_documentos_url, params: { firmas_tipos_documento: { aceptado: @firmas_tipos_documento.aceptado, descripcion: @firmas_tipos_documento.descripcion, grupo_empresa_id: @firmas_tipos_documento.grupo_empresa_id, nombre: @firmas_tipos_documento.nombre } }
    end

    assert_redirected_to firmas_tipos_documento_url(FirmasTiposDocumento.last)
  end

  test "should show firmas_tipos_documento" do
    get firmas_tipos_documento_url(@firmas_tipos_documento)
    assert_response :success
  end

  test "should get edit" do
    get edit_firmas_tipos_documento_url(@firmas_tipos_documento)
    assert_response :success
  end

  test "should update firmas_tipos_documento" do
    patch firmas_tipos_documento_url(@firmas_tipos_documento), params: { firmas_tipos_documento: { aceptado: @firmas_tipos_documento.aceptado, descripcion: @firmas_tipos_documento.descripcion, grupo_empresa_id: @firmas_tipos_documento.grupo_empresa_id, nombre: @firmas_tipos_documento.nombre } }
    assert_redirected_to firmas_tipos_documento_url(@firmas_tipos_documento)
  end

  test "should destroy firmas_tipos_documento" do
    assert_difference('FirmasTiposDocumento.count', -1) do
      delete firmas_tipos_documento_url(@firmas_tipos_documento)
    end

    assert_redirected_to firmas_tipos_documentos_url
  end
end
