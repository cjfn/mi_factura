require 'test_helper'

class GrupoEmpresasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @grupo_empresa = grupo_empresas(:one)
  end

  test "should get index" do
    get grupo_empresas_url
    assert_response :success
  end

  test "should get new" do
    get new_grupo_empresa_url
    assert_response :success
  end

  test "should create grupo_empresa" do
    assert_difference('GrupoEmpresa.count') do
      post grupo_empresas_url, params: { grupo_empresa: { direcion: @grupo_empresa.direcion, nombre: @grupo_empresa.nombre } }
    end

    assert_redirected_to grupo_empresa_url(GrupoEmpresa.last)
  end

  test "should show grupo_empresa" do
    get grupo_empresa_url(@grupo_empresa)
    assert_response :success
  end

  test "should get edit" do
    get edit_grupo_empresa_url(@grupo_empresa)
    assert_response :success
  end

  test "should update grupo_empresa" do
    patch grupo_empresa_url(@grupo_empresa), params: { grupo_empresa: { direcion: @grupo_empresa.direcion, nombre: @grupo_empresa.nombre } }
    assert_redirected_to grupo_empresa_url(@grupo_empresa)
  end

  test "should destroy grupo_empresa" do
    assert_difference('GrupoEmpresa.count', -1) do
      delete grupo_empresa_url(@grupo_empresa)
    end

    assert_redirected_to grupo_empresas_url
  end
end
