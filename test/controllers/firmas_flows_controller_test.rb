require 'test_helper'

class FirmasFlowsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @firmas_flow = firmas_flows(:one)
  end

  test "should get index" do
    get firmas_flows_url
    assert_response :success
  end

  test "should get new" do
    get new_firmas_flow_url
    assert_response :success
  end

  test "should create firmas_flow" do
    assert_difference('FirmasFlow.count') do
      post firmas_flows_url, params: { firmas_flow: { firmas_tipos_documentos_id: @firmas_flow.firmas_tipos_documentos_id, lleva_firma: @firmas_flow.lleva_firma, paso_aceptado: @firmas_flow.paso_aceptado, paso_no_aceptado: @firmas_flow.paso_no_aceptado, texto_aceptado: @firmas_flow.texto_aceptado, texto_no_aceptado: @firmas_flow.texto_no_aceptado, tipo: @firmas_flow.tipo, user_id: @firmas_flow.user_id } }
    end

    assert_redirected_to firmas_flow_url(FirmasFlow.last)
  end

  test "should show firmas_flow" do
    get firmas_flow_url(@firmas_flow)
    assert_response :success
  end

  test "should get edit" do
    get edit_firmas_flow_url(@firmas_flow)
    assert_response :success
  end

  test "should update firmas_flow" do
    patch firmas_flow_url(@firmas_flow), params: { firmas_flow: { firmas_tipos_documentos_id: @firmas_flow.firmas_tipos_documentos_id, lleva_firma: @firmas_flow.lleva_firma, paso_aceptado: @firmas_flow.paso_aceptado, paso_no_aceptado: @firmas_flow.paso_no_aceptado, texto_aceptado: @firmas_flow.texto_aceptado, texto_no_aceptado: @firmas_flow.texto_no_aceptado, tipo: @firmas_flow.tipo, user_id: @firmas_flow.user_id } }
    assert_redirected_to firmas_flow_url(@firmas_flow)
  end

  test "should destroy firmas_flow" do
    assert_difference('FirmasFlow.count', -1) do
      delete firmas_flow_url(@firmas_flow)
    end

    assert_redirected_to firmas_flows_url
  end
end
