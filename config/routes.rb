Rails.application.routes.draw do
  
  root to: 'customers#index'
  resources :customers
  resources :firmas_flows
  resources :grupo_empresas
  resources :firmas_tipos_documentos
  resources :facturas_detalles, only: [:create, :destroy] #solo puedo crear y eliminar detalle de factura

  resources :facturas do # permisos a metodo para ver pdf subido en un folder
  member do
    get :nueva_fila
  end
end
  resources :productos do # permisos a metodo para ver pdf subido en un folder
  member do
    get :muestra_pdf
  end
end
  root to: 'visitors#index'
  devise_for :users
  resources :users
end
