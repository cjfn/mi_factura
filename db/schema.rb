# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170615184851) do

  create_table "customers", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone"
    t.string   "email"
    t.string   "street"
    t.string   "zip"
    t.string   "city"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "facturas", force: :cascade do |t|
    t.integer  "numero"
    t.date     "fecha"
    t.string   "nit_cliente"
    t.string   "nombre_cliente"
    t.decimal  "total"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "facturas_detalles", force: :cascade do |t|
    t.integer  "factura_id"
    t.integer  "producto_id"
    t.integer  "cantidad"
    t.decimal  "precio"
    t.decimal  "total"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "firmas_flows", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "firmas_tipos_documentos_id"
    t.boolean  "lleva_firma"
    t.string   "tipo"
    t.integer  "paso_aceptado"
    t.string   "texto_aceptado"
    t.integer  "paso_no_aceptado"
    t.string   "texto_no_aceptado"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "firmas_tipos_documentos", force: :cascade do |t|
    t.integer  "grupo_empresa_id"
    t.string   "nombre"
    t.text     "descripcion"
    t.boolean  "aceptado"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "grupo_empresas", force: :cascade do |t|
    t.string   "nombre"
    t.text     "direcion"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "productos", force: :cascade do |t|
    t.string   "codigo"
    t.string   "descripcion"
    t.decimal  "precio"
    t.text     "observaciones"
    t.boolean  "activo"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "imagen_file_name"
    t.string   "imagen_content_type"
    t.integer  "imagen_file_size"
    t.datetime "imagen_updated_at"
    t.string   "pdf_file_name"
    t.string   "pdf_content_type"
    t.integer  "pdf_file_size"
    t.datetime "pdf_updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
