class CreateFirmasFlows < ActiveRecord::Migration[5.0]
  def change
    create_table :firmas_flows do |t|
      t.integer :user_id
      t.integer :firmas_tipos_documentos_id
      t.boolean :lleva_firma
      t.string :tipo
      t.integer :paso_aceptado
      t.string :texto_aceptado
      t.integer :paso_no_aceptado
      t.string :texto_no_aceptado

      t.timestamps
    end
  end
end
