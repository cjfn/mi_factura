class AddAttachmentPdfToProductos < ActiveRecord::Migration
  def self.up
    change_table :productos do |t|
      t.attachment :pdf
    end
  end

  def self.down
    remove_attachment :productos, :pdf
  end
end
