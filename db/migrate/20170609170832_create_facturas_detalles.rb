class CreateFacturasDetalles < ActiveRecord::Migration[5.0]
  def change
    create_table :facturas_detalles do |t|
      t.integer :factura_id
      t.integer :producto_id
      t.integer :cantidad
      t.decimal :precio
      t.decimal :total

      t.timestamps
    end
  end
end
