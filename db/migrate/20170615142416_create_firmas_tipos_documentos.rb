class CreateFirmasTiposDocumentos < ActiveRecord::Migration[5.0]
  def change
    create_table :firmas_tipos_documentos do |t|
      t.integer :grupo_empresa_id
      t.string :nombre
      t.text :descripcion
      t.boolean :aceptado

      t.timestamps
    end
  end
end
