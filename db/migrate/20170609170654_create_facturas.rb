class CreateFacturas < ActiveRecord::Migration[5.0]
  def change
    create_table :facturas do |t|
      t.integer :numero
      t.date :fecha
      t.string :nit_cliente
      t.string :nombre_cliente
      t.decimal :total

      t.timestamps
    end
  end
end
