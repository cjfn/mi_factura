class CreateGrupoEmpresas < ActiveRecord::Migration[5.0]
  def change
    create_table :grupo_empresas do |t|
      t.string :nombre
      t.text :direcion

      t.timestamps
    end
  end
end
